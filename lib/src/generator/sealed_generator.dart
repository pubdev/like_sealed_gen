import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/dart/element/type.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:like_sealed_gen/src/content/sealed_content.dart';
import 'package:like_sealed_gen/src/description/class_description.dart';
import 'package:source_gen/source_gen.dart';

class SealedGenerator extends GeneratorForAnnotation<LikeSealed> {
  @override
  dynamic generateForAnnotatedElement(
    Element element,
    ConstantReader annotation,
    _,
  ) {
    if (element is ClassElement) {
      final nullSafety = element.library.isNonNullableByDefault;
      final objectValue = annotation.objectValue;
      final likeSealed = LikeSealed(
        collection: objectValue.getField("collection")!.toBoolValue()!,
        switchInterface:
            objectValue.getField("switchInterface")!.toBoolValue()!,
        switchImpl: objectValue.getField("switchImpl")!.toBoolValue()!,
        shortName: objectValue.getField("shortName")!.toBoolValue()!,
      );

      final extendedClasses = getSubtypes(element.library, element.thisType)
          .map((v) => ClassDescription(v, nullSafety))
          .toList();

      final classDescription = ClassDescription(element, nullSafety);
      extendedClasses.add(classDescription);

      return ContentFormat(
        likeSealed,
        classDescription,
        extendedClasses,
          nullSafety,
      ).build();
    }
    return "";
  }

  List<ClassElement> getSubtypes(LibraryElement lib, InterfaceType type) {
    final types = <ClassElement>[];
    final libraryReader = LibraryReader(lib);

    libraryReader.classes.forEach((classElement) {
      if (isExtended(classElement.supertype, type)) {
        types.add(classElement);
      }
    });

    return types;
  }

  bool isExtended(InterfaceType? element, InterfaceType type) {
    if (element == null) {
      return false;
    } else if (element.hashCode == type.hashCode) {
      return true;
    } else {
      return isExtended(element.superclass, type);
    }
  }
}
