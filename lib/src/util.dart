extension StringUtil on String {
  String get firstToLower {
    if (this.isEmpty) {
      return this;
    } else {
      return this[0].toLowerCase() + this.substring(1);
    }
  }

  String get firstToUpper {
    if (this.isEmpty) {
      return this;
    } else {
      return this[0].toUpperCase() + this.substring(1);
    }
  }

  String removeIf(String startWith) {
    if (this.startsWith(startWith)) {
      return this.substring(startWith.length );
    } else {
      return this;
    }
  }
}

enum Case { Upper, Lower }
extension IterableUtil<E> on Iterable<E> {
  E? firstWhereOrNull(bool test(E element)) {
    for (E element in this) {
      if (test(element)) return element;
    }
    return null;
  }
}
