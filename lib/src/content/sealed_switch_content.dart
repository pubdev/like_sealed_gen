import 'package:analyzer/dart/element/element.dart';
import 'package:like_sealed/like_sealed.dart';
import 'package:like_sealed_gen/src/description/class_description.dart';
import 'package:like_sealed_gen/src/util.dart';

class SealedSwitchContent {
  final LikeSealed config;
  final ClassDescription parentType;
  final List<ClassDescription> types;

  SealedSwitchContent(
    this.config,
    this.parentType,
    this.types,
  );

  String content() {
    String typeChecker = "";
    String caseFunction = "";
    final hasGenerics = parentType.generics.isNotEmpty;

    final inputType = hasGenerics ? "I" : parentType.name;

    for (var type in types) {
      final typeName = type.name;
      if (typeName == parentType.name) {
        typeChecker += "onDefault(type)";
        caseFunction +=
            "O onDefault($inputType ${typeName.firstToLower}) => throw UnimplementedError();";
      } else {
        String functionName = typeName;
        if (config.shortName) {
          functionName = functionName.removeIf(parentType.name);
        }
        typeChecker += "type is $typeName? on$functionName(type): ";
        caseFunction +=
            "O on$functionName($typeName ${functionName.firstToLower});";
      }
    }
    return """
    abstract class ${parentType.name}Switch<${hasGenerics ? "I extends ${parentType.name}, " : ""}O> implements SealedSwitch<${hasGenerics ? "I" : parentType.name},O> {
      O switchCase($inputType type)=>$typeChecker;
      $caseFunction
    }
    """;
  }
}
