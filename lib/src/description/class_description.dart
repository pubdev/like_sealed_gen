import 'package:analyzer/dart/element/element.dart';
import 'package:like_sealed_gen/src/util.dart';

class ClassDescription {
  final String name;
  final List<String> generics;
  final List<ConstructorDescription> constructors;
  final bool isAbstract;

  ClassDescription._(
    this.name,
    this.generics,
    this.isAbstract,
    this.constructors,
  );

  factory ClassDescription(ClassElement element, bool nullSafety) {
    return ClassDescription._(
      element.name,
      element.typeParameters
          .map((v) => v.getDisplayString(withNullability: nullSafety))
          .toList(),
      element.isAbstract,
      element.constructors.map((v) => ConstructorDescription(v,nullSafety)).toList(),
    );
  }
}

class ConstructorDescription {
  final bool isAbstract;
  final String returnType;
  final String name;
  final List<ParametersDescription> parameters;

  ConstructorDescription._(
    this.isAbstract,
    this.returnType,
    this.name,
    this.parameters,
  );

  factory ConstructorDescription(ConstructorElement v,bool nullSafety) {
    return ConstructorDescription._(
      v.isAbstract,
      v.returnType.getDisplayString(withNullability: nullSafety),
      v.name,
      v.parameters.map((v) => ParametersDescription(v,nullSafety)).toList(),
    );
  }
}

class ParametersDescription {
  final bool isNamed;
  final String name;
  final bool isOptional;
  final String type;
  final String? importPrefix;
  final String? defaultValueCode;

  ParametersDescription._(
    this.isNamed,
    this.name,
    this.isOptional,
    this.type,
    this.importPrefix,
    this.defaultValueCode,
  );

  factory ParametersDescription(ParameterElement v,bool nullSafety) {
    return ParametersDescription._(
      v.isNamed,
      v.name,
      v.isOptional,
      v.type.getDisplayString(withNullability: nullSafety),
      _getImportPrefix(v),
      v.defaultValueCode,
    );
  }

  static String? _getImportPrefix(
    ParameterElement parameter,
  ) {
    final importIdentifier = parameter.type.element?.library?.identifier;
    if (importIdentifier == null) {
      return null;
    }
    final import = parameter.library!.imports.firstWhereOrNull(
      (element) => element.importedLibrary!.identifier == importIdentifier,
    );
    if (import?.prefix != null) {
      return import!.prefix!.name;
    }
    return null;
  }
}
