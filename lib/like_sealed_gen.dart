import 'package:build/build.dart';
import 'package:like_sealed_gen/src/generator/sealed_generator.dart';
import 'package:source_gen/source_gen.dart';

Builder builder([BuilderOptions? options]) =>
    PartBuilder([SealedGenerator()], ".sealed.dart");
