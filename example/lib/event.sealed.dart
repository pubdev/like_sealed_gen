// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class Events {
  static EventInit init() {
    return EventInit();
  }

  static EventUpdate update() {
    return EventUpdate();
  }
}

abstract class EventSwitch<O> implements SealedSwitch<Event, O> {
  O switchCase(Event type) => type is EventInit
      ? onInit(type)
      : type is EventUpdate
          ? onUpdate(type)
          : onDefault(type);
  O onInit(EventInit init);
  O onUpdate(EventUpdate update);
  O onDefault(Event event) => throw UnimplementedError();
}

class EventSwitchImpl<O> implements EventSwitch<O> {
  final O Function(EventInit) _init;
  final O Function(EventUpdate) _update;
  final O Function(Event) _event;

  EventSwitchImpl({
    O Function(EventInit) init,
    O Function(EventUpdate) update,
    O Function(Event) event,
  })  : _init = init,
        _update = update,
        _event = event;

  @Deprecated("use base constructor or required")
  EventSwitchImpl.fill(
    this._init,
    this._update,
    this._event,
  );

  EventSwitchImpl.required({
    @required O Function(EventInit) init,
    @required O Function(EventUpdate) update,
    @required O Function(Event) event,
  })  : _init = init,
        _update = update,
        _event = event;

  O switchCase(Event type) => (type is EventInit && _init != null)
      ? onInit(type)
      : (type is EventUpdate && _update != null)
          ? onUpdate(type)
          : (type is Event && _event != null)
              ? onDefault(type)
              : throw UnimplementedError();

  O onInit(EventInit value) => _init(value);
  O onUpdate(EventUpdate value) => _update(value);
  O onDefault(Event value) => _event(value);
}
