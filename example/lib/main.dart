import 'event.dart';
import 'example_bloc.dart';

main() {
  final bloc = ExampleBloc();
  bloc.mapEventToState(Events.update());
  bloc.mapEventToState(Events.init());
}
