// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// SealedGenerator
// **************************************************************************

class States {
  static StateData data(
    String data,
  ) {
    return StateData(data);
  }

  static StateError error({
    dynamic error,
  }) {
    return StateError(error: error);
  }
}

abstract class StateSwitch<O> implements SealedSwitch<State, O> {
  O switchCase(State type) => type is StateData
      ? onData(type)
      : type is StateError
          ? onError(type)
          : onDefault(type);
  O onData(StateData data);
  O onError(StateError error);
  O onDefault(State state) => throw UnimplementedError();
}
