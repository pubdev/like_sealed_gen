import 'package:like_sealed/like_sealed.dart';

part 'state.sealed.dart';

@likeSealed
abstract class State {}

class StateData extends State {
  final String data;

  StateData(this.data);
}

class StateError extends State {
  final dynamic error;

  StateError({this.error});
}
