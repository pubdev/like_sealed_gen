import 'event.dart';
import 'state.dart';

class ExampleBloc extends EventSwitch<Stream<State>> {
  Stream<State> mapEventToState(Event event) {
    return switchCase(event);
  }

  @override
  Stream<State> onInit(EventInit init) async* {
    yield States.data("data");
  }

  @override
  Stream<State> onUpdate(EventUpdate update) async* {
    yield States.error(error: "error");
  }
}
