## [0.7.2] - 07.05.2021.

* fix for packages without null safety.

## [0.7.1+1] - 22.04.2021.

* fix of SwitchImpl.

## [0.7.1] - 17.04.2021.

* add generic type in Switch and SwitchImpl.

## [0.7.0] - 16.04.2021.

* support generic types.

## [0.6.0] - 5.04.2021.

* update dependencies to null safety.

## [0.5.0] - 06.03.2021.

* null safety.

## [0.4.1] - 02.01.2021.

* support import's prefixes.

## [0.4.0] - 24.12.2020.

* now annotation support parameters.

## [0.3.1] - 7.10.2020.

* onDefault in switch return null by default.

## [0.3.0+1] - 7.10.2020.

* fix.

## [0.3.0] - 7.10.2020.

* upgrade dependency.

## [0.2.3] - 10.08.2020.

* ...SwitchImpl for fast usage.

## [0.2.2] - 06.03.2020.

* fix optional constructor parameters.

## [0.2.0] - 06.03.2020.

* Now using library builder.

## [0.1.0] - 29.02.2020.

* Initial release.

